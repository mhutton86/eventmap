var app = angular.module('EventMap');

app.controller('MainCtrl', function ($scope, $http, $log, DataService, $q, _, $mdSidenav) {

	// application data.
	$scope.companies = [];
	$scope.locations = [];
	$scope.users = [];
	$scope.events = [];

	// generated map points & promises
	$scope.mapPointsDeferred = $q.defer();
	$scope.mapPointsPromise = $scope.mapPointsDeferred.promise;
	$scope.mapPoints = [];

	// event section
	$scope.eventDetails = undefined;
	$scope.eventSelected = false;

	// Where to display the arrow
	$scope.arrowLeft = true;

	// Opens and closes the side bar
	$scope.toggleSidebar = function(id) {
		$scope.arrowLeft = !$scope.arrowLeft;
		$mdSidenav(id).toggle();
	};

	//brings user back to the all details view
	$scope.backToDetails = function () {
		$scope.eventSelected = false;
	};

	$scope.populateEvent = function (eventId) {
        var foundEvent = DataService.getEventById(eventId);

        if (!foundEvent) {
        	$log.error("No event found for ID", eventId);
        	return;
		}

		$scope.eventSelected = true;
		$scope.eventDetails = foundEvent;
	};

    /**
	 * Create the map points so the MapDirective can post them.
     */
	var createMapPoints = function () {
        var counter = 0;
        _.forEach($scope.events, function(event) {
        	var location = DataService.getLocationById(event.location);

			$scope.mapPoints.push({
				title: event.name,
				onMarkerClick: $scope.populateEvent,
				onMarkerClickArgs: [event.id],
				coordinates: [location.coordinates[0], location.coordinates[1]]
			});
            counter++;
		});
        $scope.mapPointsDeferred.resolve($scope.mapPoints);
	};

	$scope.init = function () {
		// get all the data required by this controller
		return $q.all([
			DataService.getCompanies(),
			DataService.getLocations(),
			DataService.getUsers(),
			DataService.getEvents()
		]).then(function (ret) {
			$log.log('companies', ret[0]);
			$scope.companies = ret[0];
			$log.log('locations', ret[1]);
			$scope.locations = ret[1];
			$log.log('users', ret[2]);
			$scope.users = ret[2];
			$log.log('events', ret[3]);
			$scope.events = ret[3];

			// create map points for map directive.
			createMapPoints();
		}, function (err) {
			$log.error("Data wasn't loaded", err);
		});
	};
});
