/**
 * Intention: I have a map that I can pass points and data and markers are created on the map.
 *
 * I will probably need the ability to add multiple and/or single markers
 * {
 *     template: "",
 *     coordinates: [lat, long],
 * }
 *
 * I am following the pattern defined in the Todd Motto directives article: https://toddmotto.com/killing-it-with-angular-directives-structure-and-mvvm/
 */
function gMap() {
    /**
     * @name link
     * @desc Google Map Link. For all DOM event handling.
     * @type {Function}
     */
    function link($scope, $element, $attrs, $ctrl) {
        console.log('link $scope', $scope);

        // $ctrl is the gMapCtrl above.
        var mapCanvas = $element.find('#my-map')[0];

        // call the user provided promise for points data
        $scope.mapPointsPromise.then(function (newPoints) {
            console.log('points', newPoints);

            var mapOptions = {
                center: new google.maps.LatLng(38.908585, -104.780748),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(mapCanvas, mapOptions);

            // loop through each point and add a marker to the google map
            _.forEach(newPoints, function (event) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(event.coordinates[0], event.coordinates[1]),
                    title: event.title
                });

                var contentString = event.bodyTemplate;

                // on marker click, call the user provided onMarkerClick with provided args
                marker.addListener('click', function () {
                    console.log("event", event);
                    event.onMarkerClick(event.onMarkerClickArgs[0]);
                    // this needs to be called, when this function is called from another directive/controller.
                    $scope.$apply();
                });

                marker.setMap(map);
            });
        });
    }

    return {
        restrict: 'E',
        scope: {
            mapPointsPromise: '=',
            onPointClickFunction: '&'
        },
        link: link,
        templateUrl: 'src/main/ng/modules/templates/google-map.html'
    };
}

angular
    .module('EventMap')
    .directive('gMap', gMap);

