var app = angular.module('EventMap');

/**
 * Caching DataService since we don't have an actual backend w/ real data.
 */
app.factory('DataService', function ($http, $log, $q) {

	// application data.
	this.companies = [];
	this.locations = [];
	this.users = [];
	this.events = [];

	this.initialized = false;

	this.getLocationById = function (id) {
		return _.find(this.locations, function (location) {
			return location.id === id;
		})
	};

    /**
     * @param id The ID of the event to be returned.
     * @return event for ID or null if there's no event.
     */
	this.getEventById = function (id) {
		return _.find(this.events, function (event) {
			return event.id === id;
		})
	};

	/**
	 * Initialize data for the service
	 * @returns {*} a promise of completion
	 */
	this.init = function () {
		var vm = this;

		if (!this.initialized) {

			$log.log('DataService Initializing');

			var baseDataPath = 'src/main/ng/assets/data';

			var dataUrls = [
				'/companies.json',
				'/locations.json',
				'/events.json',
				'/users.json'
			];

			var arr = [];
			for (var a = 0; a < dataUrls.length; ++a) {
				arr.push($http.get(baseDataPath + dataUrls[a]));
			}

			// reuse the promise as a result
			return $q.all(arr).then(function (ret) {
				// success!
				vm.companies = ret[0].data;
				$log.log('Companies loaded', vm.companies);
				vm.locations = ret[1].data;
				$log.log('Locations loaded', vm.locations);
				vm.events = ret[2].data;
				$log.log('Events loaded', vm.events);
				vm.users = ret[3].data;
				$log.log('Users loaded', vm.users);

				// we've successfully initialized our data.
				vm.initialized = true;
				$log.log('DataService Initialized');

				return ret;
			}, function (ret) {
				$log.error('DataService failed to Initialize');

				return ret;
			});
		}
		return $q.when();
	};

	this.whenInitialized = function() {
		if (!this.initialized) {
			return this.init().then(function () {
				return this;
			}, function () {
				return this;
			})
		} else {
			return $q.when();
		}
	};

	this.getCompanies = function () {
		var vm = this;
		return this.whenInitialized().then(function(){return vm.companies});
	};
	this.getLocations = function () {
		var vm = this;
		return this.whenInitialized().then(function(){return vm.locations});
	};
	this.getUsers = function () {
		var vm = this;
		return this.whenInitialized().then(function(){return vm.users});
	};
	this.getEvents = function () {
		var vm = this;
		return this.whenInitialized().then(function(){return vm.events});
	};

	// factories must return a reference to itself or an object containing exposed functinos
	return this;
});
