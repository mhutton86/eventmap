function LodashFactory($log, $window) {
	if(!$window._){
		// If lodash is not available you can now provide a
		// mock service, try to load it from somewhere else,
		// redirect the user to a dedicated error page, ...
		$log.error('Lodash not defined')
	}
	return $window._;
}

// Register factory
angular.module('EventMap').factory('_', LodashFactory, ['$log', '$window']);