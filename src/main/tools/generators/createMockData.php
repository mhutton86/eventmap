<?php
/*
 * createMockData.php: script used to create mock data for the Event Map application.
 */

date_default_timezone_set('America/Denver');

// random words for generating titles, names, etc...
$words = [
	'interdevour', 'amphigenous', 'wren', 'marceline', 'slype', 'nonspiny', 'forensics', 'unperjuring', 'porousness', 'thump', 'hypobaropathy', 'overqualify', 'myrmecophile', 'funest', 'wasp', 'ostectomy', 'wycliffe', 'letterman', 'prestretch', 'disseminating', 'priapic', 'pseudolabium', 'thickety', 'deviousness', 'terrorization', 'outsonnet', 'anomalistic', 'sphinxes', 'trueness', 'physicochemical', 'acetylization', 'discusser', 'participatory', 'pneumatograph', 'milwaukie', 'nonpurchaser', 'catechumenate', 'martyrish', 'braggartly', 'overvalued', 'coinsurable', 'subparietal', 'makasar', 'arsinous', 'saccharometric', 'aprioristically', 'monomethylamine', 'ryukyu', 'serialism', 'predevelop', 'reobtainable', 'participation', 'hiver', 'mayflower', 'hamperedness', 'bewray', 'cibarial', 'interlunar', 'obligator', 'eryx', 'quinhydrone', 'assimilationist', 'estivating', 'hydroxy', 'blab', 'melodie', 'oneida', 'herculean', 'septennial', 'poignantly', 'inflatable', 'scrappiness', 'letdown', 'levorotary', 'sidespin', 'prodomos', 'gilder', 'delocalised', 'penetrably', 'elegise', 'tideful', 'entozoic', 'suq', 'oos', 'valence', 'pontificate', 'prolactin', 'farnborough', 'fytte', 'balladeer', 'trenchancy', 'masseur', 'enticement', 'resignalled', 'nonnicotinic', 'plebeian', 'celebrater', 'kyat'
];

$images = [
	'beer.jpg',
	'bookSigningImg.jpg',
	'metroid.jpg',
	'nap.jpg',
	'party.jpg',
	'sun.jpg',
	'picnic.jpg'
];

$categories = [
	'sports', 'food', 'random', 'games', 'exercise'
];

$loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse iaculis justo ante, vitae porta purus ultrices dapibus. Pellentesque non molestie eros, eu tempor felis. In libero massa, faucibus non condimentum in, vulputate id enim. Sed nec tristique tortor. In a elit et enim cursus vehicula. Etiam eu finibus urna. Ut placerat, lacus et fringilla ultrices, diam est luctus quam, ac bibendum nisi nisi sed dui. Phasellus finibus vulputate purus id convallis. Donec tempus ac nunc sed placerat.\n\n
Ut maximus quis enim vel egestas. Maecenas tincidunt urna neque, dignissim laoreet risus dictum in. Mauris in vehicula leo, non rhoncus massa. Sed aliquam dui vel augue posuere, non vulputate libero iaculis. Nam hendrerit nulla quam, nec suscipit diam commodo at. Pellentesque vestibulum semper porttitor. Curabitur id erat in mi congue pulvinar non ac risus. Phasellus ac aliquet magna. Integer et lorem sed lacus tempus cursus. Vivamus feugiat blandit lorem. Curabitur vel risus eu eros porta cursus. Aenean venenatis viverra velit vel finibus.";

function randomWord() {
	global $words;

	return randomElement($words);
}

/**
 * Create some notional event date, time, and duration.
 */
function randomDateTimeBlock() {
	// ref: http://stackoverflow.com/questions/16118565/round-datetime-to-last-hour
	// randomize event start time some time in the future
	$minutesFromNow = rand(-60, 43200); // from 15 minutes before 'til a month (30*24*60)
	$now = new DateTime('now', new DateTimeZone("GMT"));
	$di = new DateInterval("PT${minutesFromNow}M");
    $startDateTime = $now->add($di)->format(DATE_ATOM);
    // randomize event end time some duration afterwards
    $durationInMinutes = 15 * rand(1, 100);
    $durationInSeconds = $durationInMinutes * 60;

    return [
		'startDateTime' => $startDateTime,
        'durationInSeconds' => $durationInSeconds
	];
}

function randomCategory() {
	global $categories;

	return randomElement($categories);
}

/** returns a random value in an array. */
function randomElement($array) {
	return $array[rand(0, count($array) - 1)];
}

/**
 * @param float $min minimum float range
 * @param float $max maximum float range
 * @return float random number between minimum and maximum number's provided.
 */
function randomFloatRange($min, $max) {
	// ensure numbers were passed in the correct order.
	if ($max < $min) {
		$tmp = $max;
		$max = $min;
		$min = $tmp;
	}
	return ($min + lcg_value() * (abs($max - $min)));
}

/**
 * Creates a random latitude, and longitude somewhere in colorado springs.
 * @return array Array of a randomly generated lat, long.
 */
function randomLatLong() {
	$minLat = 38.756431;
	$maxLat = 38.968638;
	$minLong = -104.936576;
	$maxLong = -104.672128;

	return [randomFloatRange($minLat, $maxLat), randomFloatRange($minLong, $maxLong)];
}

function printHeader($header) {
	echo "-----------------\n";
	echo "$header\n";
	echo "-----------------\n";
}

// randomly generate companies
$companies = [];
for ($i = 0; $i < 100; $i++) {
	$companies[] = [
		'id' => $i,
		'name' => sprintf("%s %s", randomWord(), randomWord()),
		'description' => $loremIpsum,
		'companyUrl' => 'https://www.example.net'
	];
}

// randomly generate locations
$locations = [];
for ($i = 0; $i < 100; $i++) {
	$locations[] = [
		'id' => $i,
		'name' => sprintf("%s %s", randomWord(), randomWord()),
		'description' => $loremIpsum,
		'coordinates' => randomLatLong()
	];
}

// Generate Events (tie in locations, companies, and events)
$events = [];
for ($i = 0; $i < 100; $i++) {
	$events[] = [
		'id' => $i,
		'name' => randomWord(),
		'description' => $loremIpsum,
		'companyId' => randomElement($companies)['id'],
		'location' => randomElement($locations)['id'],
        'time' => randomDateTimeBlock(),
		'cost' => rand(0, 1000),
		'category' => randomCategory(),
		'imgSrc' => 'src/main/ng/assets/mockImgs/' . randomElement($images)
	];
}

// Generates users, and attachs them to a random assortment of events.
$users = [];
for ($i = 0; $i < 100; $i++) {
	$users[] = [
		'id' => $i,
		'name' => randomWord() . ' ' . randomWord(),
		'interests' => [randomCategory(), randomCategory(), randomCategory()],
		'homeCoodinates' => randomLatLong(),
		'events' => [randomElement($events)['id'], randomElement($events)['id'], randomElement($events)['id'], randomElement($events)['id'], randomElement($events)['id']],
	];
}

// ensure the target file directory exists.
$resourceDirectory = __DIR__ . '/../../ng/assets/data/';
mkdir($resourceDirectory, null, true);

printHeader('Companies');
file_put_contents($resourceDirectory . '/companies.json', json_encode($companies, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
printHeader('Locations');
file_put_contents($resourceDirectory . '/locations.json', json_encode($locations, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
printHeader('Events');
file_put_contents($resourceDirectory . '/events.json', json_encode($events, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
printHeader('Users');
file_put_contents($resourceDirectory . '/users.json', json_encode($users, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
